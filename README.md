## full_oppo6771_18611-user 9 PPR1.180610.011 eng.root.20200915.143154 release-keys
- Manufacturer: oppo
- Platform: mt6771
- Codename: RMX1831
- Brand: realme
- Flavor: fuse_RMX1831-userdebug
- Release Version: 12
- Id: SQ1A.220105.002
- Incremental: eng.vijaym.20220207.074110
- Tags: test-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/raven/raven:12/SD1A.210817.015.A4/7697517:user/release-keys
- OTA version: 
- Branch: full_oppo6771_18611-user-9-PPR1.180610.011-eng.root.20200915.143154-release-keys
- Repo: realme_rmx1831_dump_12448


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
